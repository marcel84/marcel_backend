using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Marcel_backend.Models;

public class Todo
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string? Id { get; set; }

    [BsonElement("Title")]
    public string Title { get; set; } = null!;

    [BsonElement("Content")]
    public string Content { get; set; } = null!;

}
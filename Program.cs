using Marcel_backend.Models;
using Marcel_backend.Services;
using SendGrid.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

// builder.Services.AddCors(options =>
// {
//     options.AddDefaultPolicy(
//         policy =>
//         {
//             policy.WithOrigins("http://localhost:3000/");
//         });
// });

builder.Services.AddCors(options =>
    {

        options.AddDefaultPolicy(
             policy =>
             {
                 policy.WithOrigins("http://localhost:3000")
                     .AllowAnyHeader()
                     .AllowAnyMethod();
             });
    });

// builder.Services.AddCors(options =>
//     {
//         options.AddPolicy("CorsApi", builder =>
//         {
//             builder
//             .WithOrigins("http://127.0.0.1:3000", "'http://localhost:3000' ");
//             // .AllowAnyHeader()
//             // .AllowAnyMethod()
//             // .AllowCredentials();
//         });
//     });

// builder.Services.AddCors(options =>
// {
//     options.AddDefaultPolicy(
//        policy =>
//        {
//            policy.WithOrigins("http://127.0.0.1:3000")
//            .AllowAnyHeader()
//             .AllowCredentials()
//             .AllowAnyMethod();
//        });
//     // options.AddPolicy("CorsApi",
//     //   policy =>
//     //   {
//     //       policy.WithOrigins("http://localhost:3000/")
//     //                           .AllowAnyHeader()
//     //                           .AllowAnyMethod();
//     //   });
//     // options.AddPolicy(name: MyAllowSpecificOrigins,
//     //                   policy =>
//     //                   {
//     //                       policy.WithOrigins("http://localhost:300",
//     //                                           "http://www.contoso.com");
//     //                   });
// });

// builder.Services.AddCors(options =>
// {
//     // options.AddPolicy("CorsApi",
//     //    builder => builder.AllowAnyOrigin()
//     //    .AllowAnyMethod()
//     //    .AllowAnyHeader());
//     options.AddPolicy(name: MyAllowSpecificOrigins,
//                       policy =>
//                       {
//                           policy
//                           .WithOrigins("http://localhost:3000/")
//     .AllowAnyMethod()
//      .AllowAnyHeader()
//      .AllowCredentials();

//                       });
// }

builder.Services.Configure<TodoStoreDatabaseSettings>(
    builder.Configuration.GetSection("Database"));
builder.Services.AddSingleton<TodosService>();
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSendGrid(options =>
{
    options.ApiKey = builder.Configuration
    .GetSection("SendGridEmailSettings").GetValue<string>("MARC_EMAIL");
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

// app.UseCors(MyAllowSpecificOrigins);
app.UseCors();
// app.UseCors(
//   options => options.WithOrigins("*").AllowAnyMethod().AllowAnyHeader()
//       );

app.UseAuthorization();

app.MapControllers();

app.Run();

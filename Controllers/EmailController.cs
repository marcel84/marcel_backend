using Microsoft.AspNetCore.Mvc;
using SendGrid.Helpers.Mail;

namespace Dot6.SendgridMail.Controllers;

[ApiController]
[Route("[controller]")]
public class EmailController : ControllerBase
{
    private readonly SendGrid.ISendGridClient _sendGridClient;
    private readonly IConfiguration _configuration;
    public EmailController(
        SendGrid.ISendGridClient sendGridClient,
        IConfiguration configuration)
    {
        _sendGridClient = sendGridClient;
        _configuration = configuration;
    }

    [HttpGet]
    [Route("send-text-mail")]
    public async Task<IActionResult> SendPlainTextEmail(string toEmail)
    {
        string fromEmail = _configuration.GetSection("SendGridEmailSettings")
        .GetValue<string>("FromEmail");

        string fromName = _configuration.GetSection("SendGridEmailSettings")
        .GetValue<string>("FromName");

        var msg = new SendGridMessage()
        {
            From = new EmailAddress(fromEmail, fromName),
            Subject = "Plain Text Email",
            PlainTextContent = "Hello, WellCome!!!"
        };
        msg.AddTo(toEmail);
        var response = await _sendGridClient.SendEmailAsync(msg);
        string message = response.IsSuccessStatusCode ? "Email Send Successfully" :
        "Email Sending Failed";
        return Ok(message);
    }

}
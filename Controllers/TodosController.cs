using Marcel_backend.Models;
using Marcel_backend.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Marcel_backend.Controllers;

[EnableCors]
[ApiController]
[Route("api/[controller]")]

public class TodosController : ControllerBase
{
    private readonly TodosService _todosService;

    public TodosController(TodosService todosService) =>
        _todosService = todosService;


    [HttpGet]
    public async Task<List<Todo>> Get() =>
            await _todosService.GetAsync();

    [HttpGet("{id:length(24)}")]
    public async Task<ActionResult<Todo>> Get(string id)
    {


        try
        {
            Console.WriteLine("GET");
            var todo = await _todosService.GetAsync(id);

            if (todo is null)
            {
                return NotFound();
            }

            return todo;

            // return CreatedAtAction(nameof(Get), new { id = id }, todo);

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        return null;
    }

    [HttpPost]
    public async Task<IActionResult> Post(Todo newtodo)
    {
        try
        {
            Console.WriteLine(newtodo);
            await _todosService.CreateAsync(newtodo);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }

        return CreatedAtAction(nameof(Get), new { id = newtodo.Id }, newtodo);
    }

    [HttpPut("{id:length(24)}")]
    public async Task<IActionResult> Update(string id, Todo updatedtodo)
    {
        var todo = await _todosService.GetAsync(id);

        if (todo is null)
        {
            return NotFound();
        }

        updatedtodo.Id = todo.Id;

        await _todosService.UpdateAsync(id, updatedtodo);

        return NoContent();
    }

    [HttpDelete("{id:length(24)}")]
    public async Task<IActionResult> Delete(string id)
    {
        try
        {
            var todo = await _todosService.GetAsync(id);

            if (todo is null)
            {
                return NotFound();
            }

            await _todosService.RemoveAsync(id);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
        return NoContent();
    }
}